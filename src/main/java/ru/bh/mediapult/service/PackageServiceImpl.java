package ru.bh.mediapult.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bh.mediapult.dao.PackageDAO;
import ru.bh.mediapult.model.Package;

import java.util.List;

/**
 * Created by 123 on 08.05.2017.
 */

@Component
public class PackageServiceImpl implements PackageService {

    @Autowired
    private PackageDAO packageDAO;

    public List<Package> getList(int cityId, int areaId) {
        if (cityId < 0 || areaId < 0) {
            throw new IllegalArgumentException("cityId and areaId can not be negative");
        }
        return packageDAO.getList(cityId, areaId);
    }
}
