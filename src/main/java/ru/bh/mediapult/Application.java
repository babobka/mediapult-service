package ru.bh.mediapult;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.core.env.AbstractEnvironment;
import ru.bh.mediapult.service.PackageService;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;


/**
 * Created by 123 on 08.05.2017.
 */

@Configuration
@ComponentScan
public class Application {

    public static void main(String[] args) {
        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "mock");
        ApplicationContext context =
                new AnnotationConfigApplicationContext(Application.class);
        PackageService packageService = context.getBean(PackageService.class);
        System.out.println(packageService.getList(0, 0));
    }
}
