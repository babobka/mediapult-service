package ru.bh.mediapult.model;

import ru.bh.mediapult.enumeration.PageFormat;

import java.io.Serializable;

/**
 * Created by 123 on 08.05.2017.
 */
public class Package implements Serializable {

    private static final long serialVersionUID = 3964919130978780154L;

    private int id;

    private int auditory;

    private int cost;

    private PageFormat pageFormat;

    private int liftsAvailable;

    private String companyName;

    private boolean hasAddress;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAuditory() {
        return auditory;
    }

    public void setAuditory(int auditory) {
        this.auditory = auditory;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public PageFormat getPageFormat() {
        return pageFormat;
    }

    public void setPageFormat(PageFormat pageFormat) {
        this.pageFormat = pageFormat;
    }

    public int getLiftsAvailable() {
        return liftsAvailable;
    }

    public void setLiftsAvailable(int liftsAvailable) {
        this.liftsAvailable = liftsAvailable;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public boolean isHasAddress() {
        return hasAddress;
    }

    public void setHasAddress(boolean hasAddress) {
        this.hasAddress = hasAddress;
    }

    @Override
    public String toString() {
        return "Package{" +
                "id=" + id +
                ", auditory=" + auditory +
                ", cost=" + cost +
                ", pageFormat='" + pageFormat + '\'' +
                ", liftsAvailable=" + liftsAvailable +
                ", companyName='" + companyName + '\'' +
                ", hasAddress=" + hasAddress +
                '}';
    }
}
