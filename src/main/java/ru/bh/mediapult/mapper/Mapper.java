package ru.bh.mediapult.mapper;

/**
 * Created by 123 on 08.05.2017.
 */
public interface Mapper<F, T> {

    T map(F entity);
}
