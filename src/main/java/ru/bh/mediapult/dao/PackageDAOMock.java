package ru.bh.mediapult.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ru.bh.mediapult.builder.Builder;
import ru.bh.mediapult.model.Package;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by 123 on 08.05.2017.
 */

@Component
@Profile("mock")
public class PackageDAOMock implements PackageDAO {

    @Autowired
    @Qualifier("packageBuilder")
    private Builder<Package> builder;

    private static final int MAX_PACKAGES = 10;

    public List<Package> getList(int cityId, int areaId) {
        List<Package> packages = new LinkedList<Package>();
        for (int i = 0; i < MAX_PACKAGES; i++) {
            packages.add(builder.build());
        }
        return packages;
    }
}
