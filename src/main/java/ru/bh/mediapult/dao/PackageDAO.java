package ru.bh.mediapult.dao;

import ru.bh.mediapult.model.Package;

import java.util.List;

/**
 * Created by 123 on 08.05.2017.
 */
public interface PackageDAO {

    List<Package> getList(int cityId, int areaId);
}
