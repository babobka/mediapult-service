package ru.bh.mediapult.dao;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ru.bh.mediapult.model.Package;

import java.util.List;

/**
 * Created by 123 on 08.05.2017.
 */

@Component
@Profile("dev")
public class PackageDAOImpl implements PackageDAO {
    public List<Package> getList(int cityId, int areaId) {
        throw new UnsupportedOperationException();
    }
}
