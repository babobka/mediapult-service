package ru.bh.mediapult.builder;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.bh.mediapult.enumeration.PageFormat;
import ru.bh.mediapult.model.Package;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Created by 123 on 08.05.2017.
 */

@Component
@Qualifier("packageBuilder")
public class DummyPackageBuilder implements Builder<Package> {

    private AtomicInteger atomicInteger = new AtomicInteger();

    private Random random = new Random();

    private static final int MAX_AUDITORY = 100_000;

    private static final int MAX_LIFTS = 30;

    private static final int MAX_COST = 100_000;

    private static final PageFormat[] PAGE_FORMATS = PageFormat.values();

    private static final String[] COMPANY_NAMES = {"Рога и копыта", "Газпром","Промстрой инвест","Сбербанк","Макдоналдс","Геббельс и ко"};

    public Package build() {
        Package pack = new Package();
        pack.setId(atomicInteger.incrementAndGet());
        pack.setAuditory(random.nextInt(MAX_AUDITORY));
        pack.setCost(random.nextInt(MAX_COST));
        pack.setLiftsAvailable(random.nextInt(MAX_LIFTS));
        pack.setPageFormat(getRandomPageFormat());
        pack.setCompanyName(getRandomCompanyName());
        pack.setHasAddress(random.nextBoolean());
        return pack;
    }

    private PageFormat getRandomPageFormat() {
        return PAGE_FORMATS[random.nextInt(PAGE_FORMATS.length)];
    }

    private String getRandomCompanyName() {
        return COMPANY_NAMES[random.nextInt(COMPANY_NAMES.length)];
    }
}