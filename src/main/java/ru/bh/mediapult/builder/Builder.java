package ru.bh.mediapult.builder;

/**
 * Created by 123 on 08.05.2017.
 */
public interface Builder<B> {

    B build();
}
