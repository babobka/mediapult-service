package ru.bh.mediapult.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.bh.mediapult.TestApplication;

import static org.junit.Assert.assertFalse;

/**
 * Created by 123 on 08.05.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("mock")
@ContextConfiguration(classes = TestApplication.class)
public class PackageServiceTest {

    @Autowired
    private PackageService packageService;

    @Test
    public void testGetList() {
        assertFalse(packageService.getList(0, 0).isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeCity() {
        packageService.getList(-1, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeArea() {
        packageService.getList(0, -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeBoth() {
        packageService.getList(-1, -1);
    }
}
