package ru.bh.mediapult.builder;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.bh.mediapult.TestApplication;
import ru.bh.mediapult.model.Package;

import static org.junit.Assert.assertNotNull;

/**
 * Created by 123 on 08.05.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("mock")
@ContextConfiguration(classes = TestApplication.class)
public class DummyBuilderTest {

    @Autowired
    @Qualifier("packageBuilder")
    private Builder<Package> builder;

    @Test
    public void testNotNull() {
        assertNotNull(builder.build());
    }

}
