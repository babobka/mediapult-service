package ru.bh.mediapult.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.bh.mediapult.TestApplication;

import static org.junit.Assert.assertFalse;

/**
 * Created by 123 on 08.05.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("mock")
@ContextConfiguration(classes = TestApplication.class)
public class PackageDAOTest {

    @Autowired
    private PackageDAO packageDAO;

    @Test
    public void testGetList() {
        assertFalse(packageDAO.getList(0, 0).isEmpty());
    }

}
